from django.urls import path
from . import views

urlpatterns = [
    path('', views.contact),
    path('snippet/',views.snippet_detail),
    path('hasil/',views.hasil_detail),
    path('hapus/',views.delete_detail),
]

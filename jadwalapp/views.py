from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ContactForm,Snippetform
from .models import Snippet

def contact(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            hari = form.cleaned_data['hari']
            print(hari)

    form = ContactForm()
    return render(request,'form.html', {'form':form})

def snippet_detail(request):
    if request.method == 'POST':
        form = Snippetform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/jadwal/hasil/')
    else:
        form = Snippetform()
    return render(request,'form.html', {'form':form})

def hasil_detail(request):
    skejuls = Snippet.objects.all()
    return render(request, 'hasil.html', {'skejuls': skejuls})

def delete_detail(request):
    Snippet.objects.all().delete()
    return redirect('/jadwal/hasil/')
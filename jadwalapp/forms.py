from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout,Submit
from django import forms
from datetime import datetime
from .models import Snippet

class ContactForm(forms.Form):            
    hari = forms.CharField()
    tanggal_jam = forms.DateField(
        widget = forms.SelectDateWidget()
    )
    #jam = forms.TimeField(widget = forms.SelectTimeWidget())
    namakegiatan = forms.CharField(required=False)
    tempat = forms.CharField()
    kategori = forms.CharField(widget=forms.TextInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper
        self.helper.form_method = 'post'

class Snippetform(forms.ModelForm):
    class Meta:
        model = Snippet
        fields = ('hari','tanggal_jam','namakegiatan','tempat','kategori')
        #fields = ('hari','namakegiatan','tempat','kategori')
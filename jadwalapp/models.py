from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.
class Snippet(models.Model):
    hari = models.CharField(max_length=100)
    tanggal_jam = models.DateTimeField(default = timezone.now)
    namakegiatan = models.CharField(max_length=100,default='')
    tempat = models.CharField(max_length=100,default = '')
    kategori = models.CharField(max_length=100,default='')

    def __str__(self):
        return self.hari
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = "index"),
    path('profile/', views.profile, name = "profile"),
    path('reach/', views.reach, name = "reach"),
    path('about/', views.about, name = "about"),
    path('index/', views.index, name = "index"),
]
